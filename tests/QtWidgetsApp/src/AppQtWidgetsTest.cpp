#include <QLineEdit>
#include <QApplication>
#include <QTimer>
#include <QFont>
#include <QFontDatabase>
#include <QDebug>
#include <iostream>

int main(int argc, char **argv)
{
  QApplication app(argc, argv);

  QFontDatabase fontDatabase;

  std::cout << "************************************\n";
  std::cout << "application font: " << app.font().toString().toStdString() << "\n";
  std::cout << "************************************" << std::endl;

  qDebug() << "************************************";
  qDebug() << "font families: " << fontDatabase.families();
  qDebug() << "************************************";
  qDebug() << "application font: " << app.font();
  qDebug() << "************************************";

  QLineEdit le;
  le.show();
  QTimer::singleShot(20, &le, &QLineEdit::hide);
  QTimer::singleShot(100, qApp, &QApplication::quit);

  return app.exec();
}
