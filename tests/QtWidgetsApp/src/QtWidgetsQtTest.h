#include <QObject>
#include <QtTest/QtTest>

/*
 * See https://gitlab.com/scandyna/docker-images/-/issues/1
 */
class QtWidgetsQtTest : public QObject
{
 Q_OBJECT

  private slots:

    void showLineEditAndQuit();
};
