#!/bin/sh

# Packages that are required build some projects
# See for example:
# - https://gitlab.com/scandyna/mdtcommandlineparser/-/jobs/2995363444
apt-get install -y \
  libfontenc-dev libice-dev libsm-dev libxaw7-dev libxcomposite-dev libxcursor-dev \
  libxdamage-dev libxinerama-dev libxkbfile-dev libxmu-dev libxmuu-dev libxpm-dev \
  libxrandr-dev libxres-dev libxss-dev libxt-dev libxtst-dev libxv-dev libxvmc-dev \
  libxxf86vm-dev uuid-dev
