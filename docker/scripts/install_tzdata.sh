#!/bin/sh

# Need to install tzdata in non interactive mode
# See https://stackoverflow.com/questions/44331836/apt-get-install-tzdata-noninteractive
# and https://serverfault.com/questions/949991/how-to-install-tzdata-on-a-ubuntu-docker-image

# For locales, see jaredmarkell.com/docker-and-locales

apt-get update -y \
  && export DEBIAN_FRONTEND=noninteractive \
  && ln -fs /usr/share/zoneinfo/Europe/Zurich /etc/localtime \
  && apt-get install -y tzdata \
  && dpkg-reconfigure --frontend noninteractive tzdata \
  && apt-get install locales -y \
  && locale-gen fr_CH.UTF-8
