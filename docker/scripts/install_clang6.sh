#!/bin/sh

# WARNING: this will install default libc++-dev
# libc++-6 variants does not exist, so the default one is installed.
# clang 6 is now old, this script should not be used anymore
apt-get install -y \
  libtsan0 clang-6.0 libclang-6.0-dev libc++-dev libc++abi-dev
