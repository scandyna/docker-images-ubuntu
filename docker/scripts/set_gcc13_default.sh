#!/bin/sh

# https://manpages.ubuntu.com/manpages/trusty/en/man8/update-alternatives.8.html
#                             link         name path           priority
update-alternatives --install /usr/bin/gcc gcc  /usr/bin/gcc-13 100
update-alternatives --install /usr/bin/g++ g++  /usr/bin/g++-13 100
#                         name path
update-alternatives --set gcc  /usr/bin/gcc-13
update-alternatives --set g++  /usr/bin/g++-13
