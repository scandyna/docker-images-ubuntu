#!/bin/sh

cd /tmp

# Install build-wrapper
echo "Downloading build-wrapper ..."
curl -sSLo ./build-wrapper-linux-x86.zip "https://sonarcloud.io/static/cpp/build-wrapper-linux-x86.zip"

if [ $? -ne 0 ]
then
  echo "downloading build-wrapper failed"
  exit 1
fi

echo "Installing build-wrapper ..."
mkdir -p /opt/sonar/build-wrapper/bin
unzip -oj build-wrapper-linux-x86.zip -d /opt/sonar/build-wrapper/bin

if [ $? -ne 0 ]
then
  echo "installing build-wrapper failed"
  exit 1
fi

rm build-wrapper-linux-x86.zip

echo "installing build-wrapper done"


# Install sonar-scanner
# The Sonar Dockerfile can help us:
# https://github.com/SonarSource/sonar-scanner-cli-docker

SONAR_SCANNER_VERSION=6.1.0.4477

echo "Downloading sonar-scanner ..."
curl -sSLo ./sonar-scanner.zip "https://binaries.sonarsource.com/Distribution/sonar-scanner-cli/sonar-scanner-cli-${SONAR_SCANNER_VERSION}-linux-x64.zip"

if [ $? -ne 0 ]
then
  echo "downloading sonar-scanner failed"
  exit 1
fi

echo "Installing sonar-scanner ..."
unzip -o sonar-scanner.zip

if [ $? -ne 0 ]
then
  echo "installing sonar-scanner failed"
  exit 1
fi

mv sonar-scanner-${SONAR_SCANNER_VERSION}-linux-x64 /opt/sonar-scanner
rm sonar-scanner.zip

echo "installing sonar-scanner done"
