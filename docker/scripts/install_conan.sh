#!/bin/sh

# Now, by default, conan 2 will be installed. We are not ready for that yet
pip3 install conan==1.64.1 \
  && conan config set general.revisions_enabled=1
