#!/bin/sh

# pipx ensurepath adds the path to the executables to ~/.bashrc (/root/.bashrc):
# export PATH="$PATH:/root/.local/bin"
#
# This does not work because ~/.bashrc is not (completely) loaded
# when the Docker image runs from the CI runner
# See:
# - https://gitlab.com/gitlab-org/gitlab-runner/-/issues/82
# - https://gitlab.com/scandyna/docker-images-ubuntu/-/issues/11
#
# So, in the Dockerfile, add this:
# ENV PATH="$PATH:/root/.local/bin"

apt-get install -y \
  python3 python3-dev python3-venv python3-pip pipx python3-markupsafe
