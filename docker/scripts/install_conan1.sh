#!/bin/sh

# Now, by default, conan 2 will be installed. We are not ready for that yet
pipx install conan==1.64.1 \
  && /root/.local/bin/conan config set general.revisions_enabled=1
