#!/bin/sh

# This schould work for Ubuntu 18.04 and 20.04
#
# A other solution could be to use pyenv
# For a example, see conan-docker-tools image:
# https://github.com/conan-io/conan-docker-tools/blob/master/modern/base/Dockerfile

apt-get install -y \
  python3.8 python3.8-dev python3.8-venv python3-pip python3-markupsafe

# https://manpages.ubuntu.com/manpages/trusty/en/man8/update-alternatives.8.html
#                             link             name    path               priority
update-alternatives --install /usr/bin/python3 python3 /usr/bin/python3.8 100
#                         name    path
update-alternatives --set python3 /usr/bin/python3.8
