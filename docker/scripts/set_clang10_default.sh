#!/bin/sh

# https://manpages.ubuntu.com/manpages/trusty/en/man8/update-alternatives.8.html
#                             link             name    path                priority
update-alternatives --install /usr/bin/clang   clang   /usr/bin/clang-10   100
update-alternatives --install /usr/bin/clang++ clang++ /usr/bin/clang++-10 100
#                         name    path
update-alternatives --set clang   /usr/bin/clang-10
update-alternatives --set clang++ /usr/bin/clang++-10
