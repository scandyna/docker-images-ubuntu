# docker-images-ubuntu

Some Docker images used for GitLab CI/CD

[[_TOC_]]

# Usage

Here is a minimal example:
```yml
stages:
  - build

build_linux_gcc_debug:
  stage: build
  image: registry.gitlab.com/scandyna/docker-images-ubuntu/ubuntu-18.04-cpp-gui:latest
  script:
    - mkdir build
    - cd build
    - conan install -s build_type=Debug --build=missing ..
    - cmake -G"Unix Makefiles" -DCMAKE_BUILD_TYPE=Debug ..
    - make -j4
  artifacts:
    expire_in: 1 day
    paths:
      - build
```

To run tests that requires a X11 server running,
choose a image that has the required packages installed,
and launch the [Xdummy](https://xpra.org/trac/wiki/Xdummy) server:
```yml
stages:
  - test

test_linux_gcc_debug:
  stage: test
  image: registry.gitlab.com/scandyna/docker-images-ubuntu/ubuntu-18.04-cpp-gui:latest
  variables:
    DISPLAY: ":99"
  dependencies:
    - build_linux_gcc_debug
  before_script:
    - /etc/init.d/xdummy-server start
  script:
    - cd build
    - ctest --output-on-failure .
```

# Available images

## Minimal images

### ubuntu-24.04-minimal

A minimal image based on [ubuntu 24.04](https://hub.docker.com/_/ubuntu)
with some additions, like:
- UTF-8 locale

This image does not provide any libraries like Qt5,
and can be used to test deployed applications.

### ubuntu-18.04-minimal

A minimal image based on [ubuntu 18.04](https://hub.docker.com/_/ubuntu)
with some additions, like:
- UTF-8 locale
- Kitware's debian apt repository configured, so recent version of cmake can be installed

This image does not provide any libraries like Qt5,
and can be used to test deployed applications.

### ubuntu-18.04-minimal-xserver

Based on `ubuntu-18.04-minimal`,
this image has a `X server` installed to provide GUI support.
Helper scripts to start a dummy X server are also provided.

This image does not provide libraries like Qt5,
but some more low level ones, like libharfbuzz .

This image can be used to test deployed applications.

### ubuntu-20.04-minimal-xserver

This image has a `X server` installed to provide GUI support.
Helper scripts to start a dummy X server are also provided.

This image does not provide libraries like Qt5,
but some more low level ones, like libharfbuzz .

This image can be used to test deployed applications.

### ubuntu-24.04-minimal-xserver

Based on `ubuntu-24.04-minimal`,
this image has a `X server` installed to provide GUI support.
Helper scripts to start a dummy X server are also provided.

This image does not provide libraries like Qt5,
but some more low level ones, like libharfbuzz .

This image can be used to test deployed applications.

## Images providing some tools

### ubuntu-18.04-xserver-cmake

Based on `ubuntu-18.04-minimal-xserver`,
this image has also CMake installed.

This image can be used to test deployed applications.

## Images to build and test c++ software

Those images provides build tools and compilers.
They also have a `X server` installed,
as well as some low-level libraries.

### ubuntu-18.04-gcc-8

Based on `ubuntu-18.04-minimal-xserver`,
this image provides compilers and other tools
to build C and C++ software.

Main tools available:
- git
- cmake
- make
- conan
- gcc-8 and g++-8

Note that no additional conan remote is configured,
the user project has add the required ones.

### ubuntu-18.04-gcc-8-doc

Based on `ubuntu-18.04-gcc-8`,
this image add tools required to build documentation:
- doxygen
- sphinx

Note: some projects probably will have to update their `cmake.py` Sphinx based CMake API doc.
For more information, see https://gitlab.com/scandyna/docker-images-ubuntu/-/issues/2 .

Note that no additional conan remote is configured,
the user project has add the required ones.

### ubuntu-18.04-gcc-8-sonar

Based on `ubuntu-18.04-gcc-8`,
this image add tools required for [sonarcloud](https://sonarcloud.io):
- [sonar-scanner](https://docs.sonarcloud.io/advanced-setup/ci-based-analysis/sonarscanner-cli/)
- sonar build-wrapper

### ubuntu-18.04-clang-10

Based on `ubuntu-18.04-minimal-xserver`,
this image provides compilers and other tools
to build C and C++ software.

Main tools available:
- git
- cmake
- make
- conan
- clang-10, clang++-10 and related libc++

Note that no additional conan remote is configured,
the user project has add the required ones.

### ubuntu-18.04-mingw

Based on `ubuntu-18.04-minimal`,
this image provides compilers and other tools
to cross build C and C++ software to Windows.

Main tools available:
- git
- cmake
- conan
- x86_64-w64-mingw32-gcc (gcc 7 64bit, also g++ and other part of the MinGW toolchain)
- i686-w64-mingw32-gcc (gcc 7 32bit, also g++ and other part of the MinGW toolchain)

Note that no additional conan remote is configured,
the user project has add the required ones.

For a example for cross-compilation with conan,
see : https://docs.conan.io/en/latest/systems_cross_building/cross_building.html

### ubuntu-20.04-clang-11

Based on `ubuntu-20.04-minimal-xserver`,
this image provides compilers and other tools
to build C and C++ software.

Main tools available:
- git
- cmake
- make
- conan
- clang-11, clang++-11 and related libc++

Note that no additional conan remote is configured,
the user project has add the required ones.

### ubuntu-24.04-gcc-13

Based on `ubuntu-24.04-minimal-xserver`,
this image provides compilers and other tools
to build C and C++ software.

Main tools available:
- git
- cmake
- make
- Conan 1 : see https://gitlab.com/scandyna/docker-images-ubuntu/-/issues/12
- gcc-13 and g++-13

Note that no additional conan remote is configured,
the user project has to add the required ones.

### ubuntu-24.04-gcc-13-doc

Based on `ubuntu-24.04-gcc-13`,
this image add tools required to build documentation:
- doxygen
- sphinx

Note: some projects probably will have to update their `cmake.py` Sphinx based CMake API doc.
For more information, see https://gitlab.com/scandyna/docker-images-ubuntu/-/issues/2 .

Note that no additional conan remote is configured,
the user project has to add the required ones.

### ubuntu-24.04-gcc-13-sonar

Based on `ubuntu-24.04-gcc-13`,
this image add tools required for [sonarcloud](https://sonarcloud.io):
- [sonar-scanner](https://docs.sonarcloud.io/advanced-setup/ci-based-analysis/sonarscanner-cli/)
- sonar build-wrapper

Note that no additional conan remote is configured,
the user project has to add the required ones.

### ubuntu-24.04-clang-18

Based on `ubuntu-24.04-minimal-xserver`,
this image provides compilers and other tools
to build C and C++ software.

Main tools available:
- git
- cmake
- make
- Conan 1 : see https://gitlab.com/scandyna/docker-images-ubuntu/-/issues/12
- clang-18, clang++-18 and related libc++

Note that no additional conan remote is configured,
the user project has add the required ones.

## Images to build Qt

### ubuntu-18.04-build-qt-gcc-8

This image is dedicated to build Qt itself.
It has some specific packages installed.

See https://github.com/conan-io/conan-center-index/issues/4006

Note that no additional conan remote is configured,
the user project has add the required ones.

### ubuntu-18.04-build-qt-clang-10

This image is dedicated to build Qt itself.
It has some specific packages installed.

See https://github.com/conan-io/conan-center-index/issues/4006

Note that no additional conan remote is configured,
the user project has add the required ones.

### ubuntu-20.04-build-qt-clang-11

This image is dedicated to build Qt itself.
It has some specific packages installed.

See https://github.com/conan-io/conan-center-index/issues/4006

Note that no additional conan remote is configured,
the user project has add the required ones.

### ubuntu-24.04-build-qt-gcc-13

This image is dedicated to build Qt itself.
It has some specific packages installed.

See https://github.com/conan-io/conan-center-index/issues/4006

Note that no additional conan remote is configured,
the user project has to add the required ones.

### ubuntu-24.04-build-qt-clang-18

This image is dedicated to build Qt itself.
It has some specific packages installed.

See https://github.com/conan-io/conan-center-index/issues/4006

Note that no additional conan remote is configured,
the user project has to add the required ones.

# Deprecated images

Following images have been badly named, or are to old, and are deprecated.
They are still available in the registry,
but no longer maintained, and will be replaced by more specific or recent ones.

## ubuntu-20.04-xserver-clang-11

This image provides the clang compiler and other tools
to build C and C++ software.

Main tools available:
- git
- cmake
- make
- conan
- gcc-8 and g++-8
- clang-11 and libc++
- doxygen
- [sonar-scanner](https://docs.sonarcloud.io/advanced-setup/ci-based-analysis/sonarscanner-cli/)
- sonar build-wrapper

Note that no additional conan remote is configured,
the user project has add the required ones.

## ubuntu-20.04-xserver-build-qt

This image is dedicated to build Qt itself.
It has some specific packages installed.

See https://github.com/conan-io/conan-center-index/issues/4006

Note that no additional conan remote is configured,
the user project has add the required ones.

## ubuntu-18.04-xserver-build-tools

Based on `ubuntu-18.04-minimal-xserver`,
this image provides compilers and other tools
to build C and C++ software.

Main tools available:
- git
- cmake
- make
- conan
- gcc-7, g++-7, gcc-8 and g++-8
- clang and libc++
- doxygen
- [sonar-scanner](https://docs.sonarcloud.io/advanced-setup/ci-based-analysis/sonarscanner-cli/)
- sonar build-wrapper

Note that no additional conan remote is configured,
the user project has add the required ones.

## ubuntu-20.04-xserver-build-tools

This image provides compilers and other tools
to build C and C++ software.

Main tools available:
- git
- cmake
- make
- conan
- gcc-8 and g++-8
- clang and libc++
- doxygen
- [sonar-scanner](https://docs.sonarcloud.io/advanced-setup/ci-based-analysis/sonarscanner-cli/)
- sonar build-wrapper

Note that no additional conan remote is configured,
the user project has add the required ones.

## ubuntu-18.04-cpp

Based on `ubuntu-18.04-minimal`,
this image provides compilers, libraries and tools to build and test C++ software.

## ubuntu-18.04-cpp-gui

Based on `ubuntu-18.04-minimal-xserver`,
this image provides compilers, libraries and tools to build and test C++ software.

# Rationale and links

## Helpful links

- [conan-docker-tools](https://github.com/conan-io/conan-docker-tools)
- [exmaple on how to set default clang on Ubuntu](https://github.com/actions/runner-images/issues/2130)
